﻿using Enigma.Logic.Alphabets;
using Enigma.Logic.LetterSwapping;

namespace Enigma.Logic.Test
{
    [TestClass]
    public class LetterSwapperTest
    {
        public static IEnumerable<object[]> TestLetters => AlphabetProvider.AlphabetList.Select(character => new object[] {character} );

        [TestMethod]
        [DynamicData(nameof(TestLetters))]
        public void EncodeForwardEncodeBackward_AllLettersPairwiseConnected_EncodingAndDecodingChangesTheLetter(char letter)
        {
            var swapper = LetterSwapper.CreateRandom(13, 14);

            var expected = letter;
            var encoded = swapper.EncodeForward(expected);

            Assert.AreNotEqual(expected, encoded);
        }

        [TestMethod]
        [DynamicData(nameof(TestLetters))]
        public void EncodeForwardEncodeBackward_AllLettersPairwiseConnected_EncodingAndDecodingRestoresTheOriginal(char letter)
        {
            var swapper = LetterSwapper.CreateRandom(13, 14);

            var expected = letter;
            var encoded = swapper.EncodeForward(expected);
            var actual = swapper.EncodeForward(encoded);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DynamicData(nameof(TestLetters))]
        public void EncodeForward_NoConnectedLetters_NothingChanges(char letter)
        {
            var swapper = LetterSwapper.CreateRandom(0, 14);

            var expected = letter;
            var encoded = swapper.EncodeForward(expected);

            Assert.AreEqual(expected, encoded);
        }

        [TestMethod]
        [DynamicData(nameof(TestLetters))]
        public void CreateAndRestoreMemento_FullLetterMap_RestoringCreatesTheSameSwapper(char letter)
        {
            var swapper = LetterSwapper.CreateRandom(13, 14);
            var anotherSwapper = LetterSwapper.CreateRandom(13, 87);

            Assert.AreNotEqual(anotherSwapper.EncodeForward(letter), swapper.EncodeForward(letter));

            var memento = anotherSwapper.CreateMemento();
            swapper.RestoreMemento(memento);

            Assert.AreEqual(anotherSwapper.EncodeForward(letter), swapper.EncodeForward(letter));
        }
    }
}
