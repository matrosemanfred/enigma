
using Enigma.Logic.Rotors;

namespace Enigma.Logic.Test
{
    [TestClass]
    public class RotorTest
    {
#pragma warning disable CS8618 // Ein Non-Nullable-Feld muss beim Beenden des Konstruktors einen Wert ungleich NULL enthalten. Erw�gen Sie die Deklaration als Nullable.
        private Rotor _rotor;
#pragma warning restore CS8618 // Ein Non-Nullable-Feld muss beim Beenden des Konstruktors einen Wert ungleich NULL enthalten. Erw�gen Sie die Deklaration als Nullable.
        private int? seed = 14;

        [TestInitialize]
        public void Init()
        {
            _rotor = Rotor.CreateRandom(seed);
        }

        [TestMethod]
        public void TestRotate()
        {
            var before = _rotor.SecondAlphabet.ToList();
            var index1before = _rotor.EncodeForward('B');

            _rotor.Rotate();
            var index0after = _rotor.EncodeForward('A');

            CollectionAssert.AreNotEqual(before, _rotor.SecondAlphabet);
            CollectionAssert.AreEqual(before.Skip(1).ToList(), _rotor.SecondAlphabet.SkipLast(1).ToList());
            Assert.AreEqual(before.First(), _rotor.SecondAlphabet.Last());
            Assert.AreEqual(index1before, index0after);
        }

        [TestMethod]
        public void TestForwardBackwardEncode()
        {
            var expected = 'A';

            var encoded = _rotor.EncodeForward(expected);
            var decoded = _rotor.EncodeBackward(encoded);

            Assert.AreEqual(expected, decoded);
            Assert.AreNotEqual(expected, encoded);
        }

        [TestMethod]
        public void TestNotchHit()
        {
            var current = _rotor.NotchPosition;
            var rotationsToHitNotch = _rotor.NotchCount - current;

            RotateAndCheckForNoHit(rotationsToHitNotch - 1);
            RotateOnceAndAssertHit();

            RotateAndCheckForNoHit(_rotor.NotchCount - 1);
            RotateOnceAndAssertHit();
        }

        private void RotateOnceAndAssertHit()
        {
            bool didHit = false;

            _rotor.HitNotch += OnHit;
            _rotor.Rotate();
            Assert.IsTrue(didHit);
            _rotor.HitNotch -= OnHit;

            void OnHit(object? sender, EventArgs args)
            {
                didHit = true;
            }
        }

        private void RotateAndCheckForNoHit(int rotationCount)
        {
            var didHit = false;

            _rotor.HitNotch += OnHit;
            for (var i = 0; i < rotationCount; i++)
            {
                _rotor.Rotate();
                Assert.IsFalse(didHit, $"The rotator hit the notch to early. Position {i} of {rotationCount}");
            }
            _rotor.HitNotch -= OnHit;


            void OnHit(object? sender, EventArgs args)
            {
                didHit = true;
            }
        }
    }
}