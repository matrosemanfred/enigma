﻿using Enigma.Logic.Reflectors;
using Enigma.Logic.Rotors;

namespace Enigma.Logic.Test
{
    [TestClass]
    public class RotorRackTest
    {
        [TestMethod]
        public void EncodeForward_TwoRacksWithIdenticalReflector_EncodeInToTheSameOutput()
        {
            var rack_1 = RotorRack.CreateFromRotors(ReflectorCollection.UKW_A);

            char expected = 'A';
            var encoded = rack_1.EncodeForward(expected);

            var rack_2 = RotorRack.CreateFromRotors(ReflectorCollection.UKW_A);
            var decoded = rack_2.EncodeForward(expected);

            Assert.AreEqual(encoded, decoded);
        }

        [TestMethod]
        public void EncodeForward_TwoRacksWithIdenticalReflectorAndRotator_EncodeInToTheSameOutput()
        {
            var rack_1 = RotorRack.CreateFromRotors(ReflectorCollection.UKW_A, RotorCollection.I);

            char expected = 'A';
            var encoded = rack_1.EncodeForward(expected);

            var rack_2 = RotorRack.CreateFromRotors(ReflectorCollection.UKW_A, RotorCollection.I);
            var decoded = rack_2.EncodeForward(expected);

            Assert.AreEqual(encoded, decoded);
        }

        [TestMethod]
        public void EncodeForward_TwoRacksWithIdenticalReflectorAndMultipleRotators_EncodeInToTheSameOutput()
        {
            var rack_1 = RotorRack.CreateFromRotors(ReflectorCollection.UKW_A, RotorCollection.I, RotorCollection.II, RotorCollection.III);

            char expected = 'A';
            var encoded = rack_1.EncodeForward(expected);

            var rack_2 = RotorRack.CreateFromRotors(ReflectorCollection.UKW_A, RotorCollection.I, RotorCollection.II, RotorCollection.III);
            var decoded = rack_2.EncodeForward(expected);

            Assert.AreEqual(encoded, decoded);
        }

        [TestMethod]
        public void EncodeForwardEncodeBackward_EncodingDecodingWithReflector_ReturnsTheOriginalInput()
        {
            var rack_1 = RotorRack.CreateFromRotors(ReflectorCollection.UKW_A);

            char expected = 'A';
            var encoded = rack_1.EncodeForward(expected);

            var rack_2 = RotorRack.CreateFromRotors(ReflectorCollection.UKW_A);
            var decoded = rack_2.EncodeForward(encoded);

            Assert.AreEqual(expected, decoded);
        }

        [TestMethod]
        public void EncodeForwardEncodeBackward_EncodingDecodingWithReflectorAndRotator_ReturnsTheOriginalInput()
        {
            var rack_1 = RotorRack.CreateFromRotors(ReflectorCollection.UKW_A, RotorCollection.I);

            char expected = 'C';
            var encoded = rack_1.EncodeForward(expected);

            var rack_2 = RotorRack.CreateFromRotors(ReflectorCollection.UKW_A, RotorCollection.I);
            var decoded = rack_2.EncodeForward(encoded);

            Assert.AreEqual(expected, decoded);
        }


        [TestMethod]
        public void EncodeForwardEncodeBackward_EncodingDecodingWithReflectorAndTwoRotators_ReturnsTheOriginalInput()
        {
            var rack_1 = RotorRack.CreateFromRotors(ReflectorCollection.UKW_A, RotorCollection.I, RotorCollection.II);
            var rack_2 = RotorRack.CreateFromRotors(ReflectorCollection.UKW_A, RotorCollection.I, RotorCollection.II);

            char expected = 'A';
            var encoded = rack_1.EncodeForward(expected);

            var decoded = rack_2.EncodeForward(encoded);

            Assert.AreEqual(expected, decoded);
        }

        [TestMethod]
        public void EncodeForwardEncodeBackward_EncodingDecodingWithReflectorAndThreeRotators_ReturnsTheOriginalInput()
        {
            var rack_1 = RotorRack.CreateFromRotors(ReflectorCollection.UKW_A, RotorCollection.I, RotorCollection.II, RotorCollection.III);

            char expected = 'A';
            var encoded = rack_1.EncodeForward(expected);

            var rack_2 = RotorRack.CreateFromRotors(ReflectorCollection.UKW_A, RotorCollection.I, RotorCollection.II, RotorCollection.III);
            var decoded = rack_2.EncodeForward(encoded);

            Assert.AreEqual(expected, decoded);
        }

    }
}
