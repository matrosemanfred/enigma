﻿namespace Enigma
{
    /// <summary>
    /// https://www.cryptomuseum.com/crypto/enigma/wiring.htm
    /// </summary>


    public class MainViewModel : BindableBase
    {
        private string enigmaAInput;
        private string enigmaBInput;
        private string enigmaAEncoded;
        private string enigmaBEncoded;

        public MainViewModel() 
        {
            EnigmaA = Logic.Enigma.Create();
            EnigmaB = EnigmaA.Copy();

            EncodeACommand = new RelayCommand(() => EnigmaAEncoded = string.Concat(EnigmaAInput.Select(character => EnigmaA.EncodeForward(character))));
            EncodeBCommand = new RelayCommand(() => EnigmaBEncoded = string.Concat(EnigmaBInput.Select(character => EnigmaB.EncodeForward(character))));
        }

        public string EnigmaAInput 
        { 
            get => enigmaAInput; 
            set => SetValue(ref enigmaAInput, value); 
        }

        public string EnigmaBInput
        {
            get => enigmaBInput;
            set => SetValue(ref enigmaBInput, value);
        }

        public string EnigmaAEncoded
        {
            get => enigmaAEncoded;
            set => SetValue(ref enigmaAEncoded, value);
        }

        public string EnigmaBEncoded
        {
            get => enigmaBEncoded;
            set => SetValue(ref enigmaBEncoded, value);
        }

        public RelayCommand EncodeACommand { get; }
        public RelayCommand EncodeBCommand { get; }

        public Logic.Enigma EnigmaA { get; }
        public Logic.Enigma EnigmaB { get; private set; }
    }
}
