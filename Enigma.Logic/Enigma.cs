﻿using Enigma.Logic.Interfaces;
using Enigma.Logic.LetterSwapping;
using Enigma.Logic.Reflectors;
using Enigma.Logic.Rotors;

namespace Enigma.Logic
{
    public class Enigma : IEncoder, ICopyable<Enigma>
    {

        public static Enigma Create()
        {
            LetterSwapper letterSwapper = LetterSwapper.CreateRandom();
            RotorRack rotatorRack = RotorRack.CreateFromRotors(ReflectorCollection.UKW_A, RotorCollection.I, RotorCollection.II, RotorCollection.III);

            return new Enigma(letterSwapper, rotatorRack);
        }
        private Enigma(LetterSwapper letterSwapper, RotorRack rotatorRack)
        {
            LetterSwapper = letterSwapper;
            RotatorRack = rotatorRack;
        }


        private LetterSwapper LetterSwapper { get; }
        private RotorRack RotatorRack { get; }

        public char EncodeForward(char input)
        {

            var afterLetterSwap = LetterSwapper.EncodeForward(char.ToUpper(input));
            var afterRotatorRack = RotatorRack.EncodeForward(afterLetterSwap);
            var afterSwappingBack = LetterSwapper.EncodeForward(afterRotatorRack);

            return afterSwappingBack;
        }


        public EnigmaMemento CreateMemento()
        {
            return new EnigmaMemento(LetterSwapper.CreateMemento(), RotatorRack.CreateMemento());
        }

        public void RestoreMemento(EnigmaMemento memento)
        {
            LetterSwapper.RestoreMemento(memento.LetterSwapperMemento);
            RotatorRack.RestoreMemento(memento.RotorRackMemento);
        }

        public Enigma Copy()
        {
            return new Enigma(LetterSwapper.Copy(), RotatorRack.Copy());
        }
    }
}
