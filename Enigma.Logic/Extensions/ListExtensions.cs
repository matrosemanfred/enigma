﻿namespace Enigma.Logic.Extensions
{
    internal static class ListExtensions
    {
        private static Random Random = new Random();

        public static void Shuffle<T>(this IList<T> list, Random rng = null)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                var k = (rng ?? Random).Next(n + 1);
                (list[n], list[k]) = (list[k], list[n]);
            }
        }
    }
}
