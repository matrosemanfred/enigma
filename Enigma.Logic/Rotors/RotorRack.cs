﻿using Enigma.Logic.Interfaces;
using Enigma.Logic.Reflectors;

namespace Enigma.Logic.Rotors;

internal class RotorRack : IEncoder, ICopyable<RotorRack>
{
    internal static RotorRack CreateFromRotors(Reflector reflector, params Rotor[] rotators)
    {
        return new RotorRack(rotators, reflector);
    }

    internal static RotorRack Create(int rotatorCount = 3, int? randomSeed = null)
    {
        var random = randomSeed != null ? new Random(randomSeed.Value) : new Random();

        IEnumerable<Rotor> encoders = Enumerable.Range(0, rotatorCount).Select(offset => Rotor.CreateRandom(random.Next()));
        var reflector = Reflector.CreateRandom(random.Next());
        return new RotorRack(encoders, reflector);
    }
    private RotorRack(IEnumerable<Rotor> encoders, Reflector reflector)
    {
        Rotors = encoders.ToList();
        Reflector = reflector;
        ConnectEncoders();
    }

    private void ConnectEncoders()
    {
        var connectedEncoderPairs = Rotors.SkipLast(1)
                        .Zip(Rotors.Skip(1));
        foreach (var pair in connectedEncoderPairs)
        {
            pair.First.HitNotch += (_, _) => pair.Second.Rotate();
        }
    }

    public List<Rotor> Rotors { get; }
    public Reflector Reflector { get; }

    public char EncodeForward(char input)
    {
        var forwardResult = Rotors.Aggregate(input, (value, encoder) =>
        {
            return encoder.EncodeForward(value);
        });

        var forwardResultInverted = Reflector.EncodeForward(forwardResult);

        var backwardResult = Rotors.Reverse<Rotor>().Aggregate(forwardResultInverted, (value, encoder) => encoder.EncodeBackward(value));

        if (Rotors.Any())
            Rotors.First().Rotate();

        return backwardResult;
    }

    public RotorRackMemento CreateMemento()
    {
        return new RotorRackMemento(Rotors.Select(encoder => encoder.CreateMemento()).ToList(), Reflector.CreateMemento());
    }

    public void RestoreMemento(RotorRackMemento memento)
    {
        if (memento is null)
        {
            throw new ArgumentNullException(nameof(memento));
        }

        CheckIsValidMemento(memento);

        foreach ((var rotor, var rotorMemento)in Rotors.Zip(memento.RotorMementos))
        {
            rotor.RestoreMemento(rotorMemento);
        }

        Reflector.RestoreMemento(memento.ReflectorMemento);
    }

    private void CheckIsValidMemento(RotorRackMemento memento)
    {
        if(memento.RotorMementos.Count !=  Rotors.Count)
        {
            throw new InvalidOperationException("The given memento cannot be used to for this RotorRack.");
        }
    }

    public RotorRack Copy()
    {
        return new RotorRack(Rotors.Select(encoder => encoder.Copy()), Reflector.Copy());
    }
}
