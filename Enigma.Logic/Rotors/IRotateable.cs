﻿namespace Enigma.Logic.Rotors
{
    internal interface IRotateable
    {
        public void Rotate();

        public event EventHandler HitNotch;
    }
}
