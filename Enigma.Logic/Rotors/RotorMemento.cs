﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enigma.Logic.Rotors
{
    public class RotorMemento
    {
        public RotorMemento(List<char> firstAlphabet, List<char> secondAlphabet, int notchPosition)
        {
            FirstAlphabet = firstAlphabet;
            SecondAlphabet = secondAlphabet;
            NotchPosition = notchPosition;
        }

        public List<char> FirstAlphabet { get; }
        public List<char> SecondAlphabet { get; }
        public int NotchPosition { get; }
    }
}
