﻿using Enigma.Logic.Alphabets;
using Enigma.Logic.Extensions;
using Enigma.Logic.Interfaces;

namespace Enigma.Logic.Rotors
{
    internal class Rotor : IRotateableTwoWayEncoder, ICopyable<Rotor>
    {
        private int notchCount;

        public event EventHandler HitNotch;

        public int NotchCount => FirstAlphabet.Count;
        public int NotchPosition
        {
            get => notchCount;
            private set
            {
                notchCount = value;
                if (notchCount == FirstAlphabet.Count)
                {
                    notchCount = 0;
                    HitNotch?.Invoke(this, EventArgs.Empty);
                }
            }
        }
        private Rotor(List<char> firstAlphabet, List<char> secondAlphabet, int initialPosition = 0)
        {
            FirstAlphabet = firstAlphabet;
            SecondAlphabet = secondAlphabet;

            NotchPosition = initialPosition;
        }

        public List<char> FirstAlphabet { get; private set; }
        public List<char> SecondAlphabet { get; private set; }

        public static Rotor CreateFromAlphabet(List<char> first, List<char> second, int initialPosition)
        {
            return new Rotor(first, second, initialPosition);
        }

        public static Rotor CreateFromAlphabet(string first, string second, int initialPosition)
        {
            return new Rotor(first.ToCharArray().ToList(),
                second.ToCharArray().ToList(),
                initialPosition);
        }
        public static Rotor CreateRandom(int? seed = null)
        {
            var random = seed != null
                ? new Random(seed.Value)
                : new Random();

            var firstAlphabet = AlphabetProvider.AlphabetList;
            var secondAlphabet = AlphabetProvider.AlphabetList;

            do
            {
                secondAlphabet.Shuffle();
            }
            while (firstAlphabet.Zip(secondAlphabet).Any((itemsAtIndex) => itemsAtIndex.First == itemsAtIndex.Second));

            return new Rotor(firstAlphabet, secondAlphabet, random.Next() % firstAlphabet.Count);
        }

        public void Rotate()
        {
            var first = SecondAlphabet.First();
            SecondAlphabet = SecondAlphabet.Skip(1)
                .Append(first)
                .ToList();

            NotchPosition++;
        }

        public char EncodeBackward(char input)
        {
            return FirstAlphabet[SecondAlphabet.IndexOf(input)];
        }

        public char EncodeForward(char input)
        {
            return SecondAlphabet[FirstAlphabet.IndexOf(input)];
        }

        public RotorMemento CreateMemento()
        {
            return new RotorMemento(FirstAlphabet, SecondAlphabet, NotchPosition);
        }

        public void RestoreMemento(RotorMemento memento)
        {
            FirstAlphabet = memento.FirstAlphabet;
            SecondAlphabet = memento.SecondAlphabet;
            NotchPosition = memento.NotchPosition;
        }

        public Rotor Copy()
        {
            return new Rotor(FirstAlphabet, SecondAlphabet, NotchPosition);
        }
    }
}
