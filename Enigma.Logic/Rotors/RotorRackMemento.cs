﻿using Enigma.Logic.Reflectors;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enigma.Logic.Rotors
{
    public class RotorRackMemento
    {
        public RotorRackMemento(List<RotorMemento> rotorMementos, ReflectorMemento reflectorMemento)
        {
            RotorMementos = rotorMementos;
            ReflectorMemento = reflectorMemento;
        }

        public List<RotorMemento> RotorMementos { get; }
        public ReflectorMemento ReflectorMemento { get; }
    }
}
