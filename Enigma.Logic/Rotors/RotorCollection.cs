﻿using Enigma.Logic.Alphabets;

namespace Enigma.Logic.Rotors
{
    internal static class RotorCollection
    {
        public static Rotor I => Rotor.CreateFromAlphabet(AlphabetProvider.Alphabet, "EKMFLGDQVZNTOWYHXUSPAIBRCJ", 0);
        public static Rotor II => Rotor.CreateFromAlphabet(AlphabetProvider.Alphabet, "AJDKSIRUXBLHWTMCQGZNPYFVOE", 0);
        public static Rotor III => Rotor.CreateFromAlphabet(AlphabetProvider.Alphabet, "BDFHJLCPRTXVZNYEIWGAKMUSQO", 0);
        public static Rotor IV => Rotor.CreateFromAlphabet(AlphabetProvider.Alphabet, "ESOVPZJAYQUIRHXLNFTGKDCMWB", 0);
        public static Rotor V => Rotor.CreateFromAlphabet(AlphabetProvider.Alphabet, "VZBRGITYUPSDNHLXAWMJQOFECK", 0);
    }
}
