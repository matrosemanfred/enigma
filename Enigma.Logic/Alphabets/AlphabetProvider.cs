﻿namespace Enigma.Logic.Alphabets
{
    internal class AlphabetProvider
    {

        private static List<char> alphabetArray = [.. "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray()];

        public static string Alphabet { get; } = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        public static List<char> AlphabetList
        {
            get => alphabetArray.ToList();
        }
    }
}
