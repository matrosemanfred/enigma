﻿using Enigma.Logic.LetterSwapping;
using Enigma.Logic.Rotors;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enigma.Logic
{
    public class EnigmaMemento
    {
        public EnigmaMemento(LetterSwapperMemento letterSwapperMemento, RotorRackMemento rotorRackMemento)
        {
            LetterSwapperMemento = letterSwapperMemento;
            RotorRackMemento = rotorRackMemento;
        }

        public LetterSwapperMemento LetterSwapperMemento { get; }
        public RotorRackMemento RotorRackMemento { get; }
    }
}
