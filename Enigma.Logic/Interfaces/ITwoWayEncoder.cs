﻿namespace Enigma.Logic.Interfaces
{
    internal interface ITwoWayEncoder : IEncoder
    {
        char EncodeBackward(char input);

    }
}
