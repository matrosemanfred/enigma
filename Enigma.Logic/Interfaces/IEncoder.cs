﻿namespace Enigma.Logic.Interfaces
{
    internal interface IEncoder
    {
        char EncodeForward(char input);
    }
}
