﻿namespace Enigma.Logic.Interfaces
{
    internal interface ICopyable<T>
    {
        public T Copy();
    }
}