﻿using Enigma.Logic.Rotors;

namespace Enigma.Logic.Interfaces
{
    internal interface IRotateableTwoWayEncoder : ITwoWayEncoder, IRotateable
    {
    }
}