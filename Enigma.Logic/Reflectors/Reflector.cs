﻿using Enigma.Logic.Alphabets;
using Enigma.Logic.Extensions;
using Enigma.Logic.Interfaces;

namespace Enigma.Logic.Reflectors
{
    internal class Reflector : IEncoder, ICopyable<Reflector>

    {
        public List<char> FirstAlphabet { get; private set; }
        public List<char> SecondAlphabet { get; private set; }

        public static Reflector CreateRandom(int? seed = null)
        {
            var random = seed != null
                ? new Random(seed.Value)
                : new Random();

            var firstAlphabet = AlphabetProvider.AlphabetList;
            var secondAlphabet = AlphabetProvider.AlphabetList;

            do
            {
                secondAlphabet.Shuffle();
            }
            while (firstAlphabet.Zip(secondAlphabet).Any((itemsAtIndex) => itemsAtIndex.First == itemsAtIndex.Second));

            return new Reflector(firstAlphabet, secondAlphabet);
        }

        private Reflector(List<char> firstAlphabet, List<char> secondAlphabet)
        {
            FirstAlphabet = firstAlphabet;
            SecondAlphabet = secondAlphabet;

        }

        public char EncodeForward(char input)
        {
            return SecondAlphabet[FirstAlphabet.IndexOf(input)];
        }
        public Reflector Copy()
        {
            return new Reflector(FirstAlphabet.ToList(), SecondAlphabet.ToList());
        }

        public ReflectorMemento CreateMemento()
        {
            return new ReflectorMemento(FirstAlphabet.ToList(), SecondAlphabet.ToList());
        }

        public void RestoreMemento(ReflectorMemento memento)
        {
            FirstAlphabet = memento.FirstAlphabet;
            SecondAlphabet = memento.SecondAlphabet;
        }

        internal static Reflector CreateFromAlphabet(string first, string second)
        {
            return new Reflector(first.ToCharArray().ToList(), second.ToCharArray().ToList());
        }
    }
}
