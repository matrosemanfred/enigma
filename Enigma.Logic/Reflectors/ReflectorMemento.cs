﻿namespace Enigma.Logic.Reflectors
{
    public class ReflectorMemento
    {
        public ReflectorMemento(List<char> firstAlphabet, List<char> secondAlphabet)
        {
            FirstAlphabet = firstAlphabet;
            SecondAlphabet = secondAlphabet;
        }

        public List<char> FirstAlphabet { get; }
        public List<char> SecondAlphabet { get; }
    }
}
