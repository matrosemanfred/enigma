﻿using Enigma.Logic.Alphabets;

namespace Enigma.Logic.Reflectors
{
    internal static class ReflectorCollection
    {
        internal static Reflector UKW_A => Reflector.CreateFromAlphabet(AlphabetProvider.Alphabet, "EJMZALYXVBWFCRQUONTSPIKHGD");
        internal static Reflector UKW_B => Reflector.CreateFromAlphabet(AlphabetProvider.Alphabet, "YRUHQSLDPXNGOKMIEBFZCWVJAT");
        internal static Reflector UKW_C => Reflector.CreateFromAlphabet(AlphabetProvider.Alphabet, "FVPJIAOYEDRZXWGCTKUQSBNMHL");
    }
}
