﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enigma.Logic.LetterSwapping
{
    public class LetterSwapperMemento
    {
        public LetterSwapperMemento(Dictionary<char, char> letterMap)
        {
            LetterMap = letterMap;
        }

        public Dictionary<char, char> LetterMap { get; }
    }
}
