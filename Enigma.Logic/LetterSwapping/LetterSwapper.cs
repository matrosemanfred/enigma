﻿using Enigma.Logic.Alphabets;
using Enigma.Logic.Interfaces;

namespace Enigma.Logic.LetterSwapping
{
    internal class LetterSwapper : IEncoder, ICopyable<LetterSwapper>
    {
        internal static LetterSwapper CreateRandom(int connectedPairCount = 10, int? randomSeed = null)
        {
            var alphabet = AlphabetProvider.AlphabetList;
            var random = randomSeed == null ? new Random() : new Random(randomSeed.Value);

            var pairs = Enumerable.Range(0, connectedPairCount)
                .Select(_ =>
                {
                    var firstIndex = random.Next(alphabet.Count);
                    var firstCharacter = alphabet[firstIndex];
                    alphabet.RemoveAt(firstIndex);

                    var secondIndex = random.Next(alphabet.Count);
                    var secondCharacter = alphabet[secondIndex];
                    alphabet.RemoveAt(secondIndex);

                    return new Tuple<char, char>(firstCharacter, secondCharacter);
                });

            return new LetterSwapper(pairs);
        }

        private LetterSwapper(IEnumerable<Tuple<char, char>> pairs)
        {
            var letterMap = new Dictionary<char, char>();
            foreach (var pair in pairs)
            {
                letterMap.Add(pair.Item1, pair.Item2);
                letterMap.Add(pair.Item2, pair.Item1);
            }

            LetterMap = letterMap;
        }

        private LetterSwapper(Dictionary<char, char> letterMap)
        {
            LetterMap = letterMap.ToDictionary();
        }

        public Dictionary<char, char> LetterMap { get; private set; }

        public char EncodeForward(char input)
        {
            if (LetterMap.TryGetValue(input, out var result))
                return result;
            return input;
        }

        public LetterSwapperMemento CreateMemento()
        {
            return new LetterSwapperMemento(LetterMap.ToDictionary());
        }

        public void RestoreMemento(LetterSwapperMemento memento)
        {
            LetterMap = memento.LetterMap.ToDictionary();
        }

        public LetterSwapper Copy()
        {
            return new LetterSwapper(LetterMap.ToDictionary());
        }
    }
}
