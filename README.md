# Engima

Enigma is a simple private hands on project where I rebuild the encryption mechanisms that where used in WWII by the germans in their Enigma machine.
The project is written in C# and split between a WPF desktop application and the Engima.Logic project which holds the Engima logic itself and can be used in other contexts, too.

## Platform

Currently the project is setup to run as an WPF desktop application and therefor the projects are set up to run with .NET 8.0 under windows.

## Usage

This project is currently in an very early stage and to use it you might just clone the repo and compile it for yourself.


## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)